class RelationshipMailer < ApplicationMailer

  def follow_notification(user, follower)
    @user = user
    @follower = follower
    mail to: user.email, subject: "#{@follower.name} followed you"
  end

  def favorite_notification(user, favorite)
    @user = user
    @favorite = favorite
    mail to: user.email, subject: "#{@favorite.name} liked it"
  end
end
