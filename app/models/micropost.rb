class Micropost < ApplicationRecord
  belongs_to :user
  has_one_attached :image
  has_many :likes, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :notifications, dependent: :destroy
  has_many :favorite_users, through: :likes, source: :user
  default_scope -> {self.order(created_at: :desc)}
  validates :user_id,   presence: true
  validates :content,   presence: true,
                          length: { maximum: 140}
  validates :image, content_type: {in: %w[image/jpeg image/gif image/png],
                              message: "must be avalid image format"},
                                 size: {less_than: 5.megabytes,
                              message: "should be less than 5MB"}

  def display_image
    image.variant(resize_to_limit: [500, 500])
  end

  def favorite(user)
    likes.create(user_id: user.id)
  end

  def unfavorite(user)
    likes.find_by(user_id: user.id).destroy
  end

  def favorite?(user)
    favorite_users.include?(user)
  end

  def create_notification_like(current_user)
    temp = Notification.where(["visitor_id = ? and visited_id = ? and micropost_id = ? and action = ?",
                                                             current_user.id, user_id, id, 'like'])
    if temp.blank?
      notification = current_user.active_notifications.new(micropost_id: id,
                                                           visited_id: user_id,
                                                           action: 'like')
      if notification.visitor_id == notification.visited_id
        notification.checked = true
      end
      notification.save if notification.valid?
    end
  end

  def create_notification_comment(current_user, comment_id)
    temp_ids = Comment.select(:user_id).where(micropost_id: id).where.not(user_id: current_user.id).distinct
    temp_ids.each do |temp_id|
      save_notification_comment(current_user, comment_id, temp_id['user_id'])
    end
    save_notification_comment(current_user, comment_id, user_id) if temp_ids.blank?
  end

  def save_notification_comment(current_user, comment_id, visited_id)
    notification = current_user.active_notifications.new(micropost_id: id,
                                                         comment_id: comment_id,
                                                         visited_id: visited_id,
                                                         action: 'comment')
    if notification.visitor_id == notification.visited_id
      notification.checked = true
    end
    notification.save if notification.valid?
  end
end